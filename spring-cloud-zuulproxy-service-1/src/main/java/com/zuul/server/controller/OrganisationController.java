package com.zuul.server.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class OrganisationController {

	@GetMapping(value="/message")
	public String sentMessage() {
		return "Wavelabs Technologies";

	}
}
