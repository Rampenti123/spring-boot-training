package com.zuul.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringCloudZoolproxyService1Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringCloudZoolproxyService1Application.class, args);
	}

}
