package com.zuul.server.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class EmployeeController {

	@GetMapping("/employee/{id}")
	public String giveName(@PathVariable int id) {
		return "Ramakrishna onboarded with : " + id;
	}

	@GetMapping("/performence")
	public String giveName() {
		return "Good";
	}
}
