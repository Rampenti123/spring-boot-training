package com.zuul.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringCloudZoolproxyService2Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringCloudZoolproxyService2Application.class, args);
	}

}
